// To access both Body and Marker data from the vicon stream
// Built from legacy RAVEN_data_stream & applicationY
// josh redding
// jredding@mit.edu

// Includes
#include "utilities/tcpPort.hpp"
#include "utilities/thread.hpp"
#include "utilities/viconClasses.hpp"
#include "requestChannelInfo.hpp"
#include "relayBodyData.hpp"

// ROS Includes
#include <ros/ros.h> // standard ros header

// Body data log file handle
FILE* fp_bodies;
FILE* fp_markers;

// Control-C handler
//bool DONE = false;
//void controlC(int sig);

// Main block
int main( int argc, char *argv[] )
{

		// Initialize ROS
		ros::init(argc, argv, "IQBroadcastLinux");

        //## Welcome screen
        printf("Starting RAVEN_datastream...\n");

        //## Setup the CTRL-C trap
        //signal(SIGINT, controlC);

        //## Input flags
        bool flag_dcm     = false;
        bool flag_markers = false;
        bool flag_logdata = false;
	bool flag_udp     = true;

        //## Handle inputs
        for( int ii=1; ii<argc; ii++ ) {

                if( strcmp(argv[ii],"-d") == 0 ) {
                        printf(" >> Including the DCM in the output\n");
                        flag_dcm = true;
                } else if( strcmp(argv[ii],"-nb") == 0 ) {
                        printf(" >> NOT broadcasting over UDP\n");
                        flag_udp = false;
                } else if( strcmp(argv[ii],"-m") == 0 ) {
                        printf(" >> Sending single marker data over port 8888\n");
                        flag_markers = true;
                } else if( strcmp(argv[ii],"-w") == 0 ) {
                        printf(" >> Writing output to datalog.txt\n");
                        fp_bodies  = fopen("datalog_bodies.txt","w");
                        fp_markers = fopen("datalog_markers.txt","w");
                        flag_logdata = true;
                } else {
                        printf("Unrecognized input option\n");
                        printf("Input options are:\n  '-d' to include the DCM in the output \n  '-a' to include accelerations in the output \n  '-m' to publish marker data via UDP on port 8888 \n  '-w' to log the output in 'datalog.txt'\n");
                }
        }

        //## TCP socket (Query/Receive)
        TCPPort tcp;
        if( tcp.tcpInit("172.29.109.123",800) < 0 ) { //192.168.2.3",800) < 0 ) {
                printf("ERROR :: Could not configure TCP port, exiting...\n");
                return(0);
        }

        //## Variables for tracked bodies
        std::vector< MarkerChannel > MarkerChannels;
        std::vector< BodyChannel >   BodyChannels;
        int FrameChannel;

        //## Start outer loop - restarts here whenever the # of tracked bodies changes
        while( ros::ok() ) {

                //## Initialize stuff
                printf("Initializing Connection to Vicon...\n");
                MarkerChannels.clear();
                BodyChannels.clear();
                FrameChannel = 0;

                //## Request Channel Information
                int infoSize = requestChannelInfo(tcp,MarkerChannels,BodyChannels,FrameChannel);

                //## Continuously get and send body data over UDP port 9999, until numTrackedBodies changes
                relayBodyData(tcp,MarkerChannels,BodyChannels,FrameChannel,infoSize,flag_dcm,flag_udp,flag_logdata);

                //## Sleep for a bit - 200 milliseconds
                usleep(200000);
        }

        // Exit nicely
        printf("Exiting RAVEN_datastream...\n");
        tcp.tcpClose();
        if( flag_logdata ) {
                fclose(fp_bodies);
                fclose(fp_markers);
        }
        sleep(1);
        return(1);
}

//## Control-C handler
//void controlC(int sig)
//{
//        printf("EXITING DUE TO CTL-C\n");
//        DONE = true;
//}
