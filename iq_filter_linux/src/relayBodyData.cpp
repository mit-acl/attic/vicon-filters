#include "relayBodyData.hpp"

// externs from main.cpp
//extern bool DONE;
extern FILE* fp_bodies;

// statics for 'relayBodyData' only
std::vector<double> data_prev;
double ax_prev, ay_prev, az_prev;
double flip;

//## From Wikipedia - http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
void quaternion2Euler(geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw)
{
  double q0, q1, q2, q3;
  q0 = q.w;
  q1 = q.x;
  q2 = q.y;
  q3 = q.z;
  roll = atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2)) * 180 / PI;
  pitch = asin(2 * (q0 * q2 - q3 * q1)) * 180 / PI;
  yaw = atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3)) * 180 / PI;
}

void relayBodyData(TCPPort &tcp, std::vector<MarkerChannel> &MarkerChannels,
                   std::vector<BodyChannel> &BodyChannels, int &FrameChannel, int infoSize,
                   bool flag_dcm, bool flag_udp, bool flag_logdata)
{

		// Initialize broadcast node for sending the data out
		ros::NodeHandle nh;

		// Initialize vector of ROS publishers
		std::vector<ros::Publisher> pose_pub;
		std::vector<ros::Publisher> twist_pub;
		std::vector<ros::Publisher> accel_pub;

        /******************  PREPARE TO GET DATA  *********************/
        //  Get the data using the request/reply protocol.
        std::vector< double > data;
        data.resize(infoSize);
        data_prev = data;
        flip = 1.0;

        const int bufferSize = 50000;
        char buff[bufferSize];
        char *pBuff;

        double timestamp;
        double timestamp_prev = 0.0;

        std::vector< MarkerData > markerPositions;
        markerPositions.resize(MarkerChannels.size());

        std::vector< BodyData > bodyPositions;
        bodyPositions.resize(BodyChannels.size());

		// Resize ROS publishers
		pose_pub.resize(BodyChannels.size());
		twist_pub.resize(BodyChannels.size());
		accel_pub.resize(BodyChannels.size());

        /******************  KALMAN FILTER SETUP  *********************/
        // added by sahrens 10/3/2007 (adapted from bbethke's vision_estimation project)
        printf("Initializing Kalman filter...\n");
        selectKVectorContext(createKVectorContext(" ", "[ ", " ];", 4));
        selectKMatrixContext(createKMatrixContext(" ", " ;\n  ", "[ ", " ];", 4));

        // Initial state covariance matrix
        std::vector< BodyChannel >::iterator iBody;
        std::vector< BodyData >::iterator iBodyData;
        int SubjectNum = -1;
        for(iBody = BodyChannels.begin(), iBodyData = bodyPositions.begin(); iBodyData != bodyPositions.end(); iBodyData++) {
                SubjectNum++;
        		double p = 1;
                static const double P0_[] = {
                                                    p,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                    0,p,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                    0,0,p,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,p,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,p,0,0,0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,p,0,0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,p,0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,p,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,p,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,p,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,p,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,0,p,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,0,0,p,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,0,0,0,p,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,p,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,p,0,
                                                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,p,
                                            };
                Matrix P0(17, 17, P0_);

                // Initial state estimate
                Vector x(17);
                x(1)  = 0;  // x (m)
                x(2)  = 0;  // y (m)
                x(3)  = 0;  // z (m)
                x(4)  = 0;  // xdot (m/s)
                x(5)  = 0;  // ydot (m/s)
                x(6)  = 0;  // zdot (m/s)
                x(7)  = 0;  // xddot (m/s^2)
                x(8)  = 0;  // yddot (m/s^2)
                x(9)  = 0;  // zddot (m/s^2)
                x(10) = 1;  // qo
                x(11) = 0;  // qx
                x(12) = 0;  // qy
                x(13) = 0;  // qz
                x(14) = 0;  // qo_dot
                x(15) = 0;  // qx_dot
                x(16) = 0;  // qy_dot
                x(17) = 0;  // qz_dot

                // Initialize the filter
                iBodyData->filter.init(x,P0);

    			// Initialize ROS topics for broadcasting data
                int loc = iBody->Name.find(":", 0);
				std::string thisName = iBody->Name.substr(0, loc);
    			std::string posename = "/pose";
    			posename = thisName + posename;
    			std::string velname = "/vel";
    			velname = thisName + velname;
    			std::string accelname = "/accel";
    			accelname = thisName + accelname;

    			std::cout << std::endl << std::endl << "Broadcasting data for:" << std::endl;
    			std::cout << posename << std::endl;
    			std::cout << velname << std::endl;
    			std::cout << accelname << std::endl;

    			pose_pub[SubjectNum] = nh.advertise<geometry_msgs::PoseStamped>(posename, 0);
    			twist_pub[SubjectNum] = nh.advertise<geometry_msgs::TwistStamped>(velname, 0);
    			accel_pub[SubjectNum] = nh.advertise<geometry_msgs::Vector3Stamped>(accelname, 0);
        }
        // Observation vector
        Vector z_K(7);  // (x,y,z,qo,qx,qy,qz) in meters and radians

        // Control vector (N/A for this filter, but still needed for the step() function
        Vector u_K(0);

        Vector xest(17);
        Matrix Pest(17,17);
        printf("Finished Kalman setup.\n");

        /****************  END KALMAN FILTER SETUP  *******************/


        // Start Loop here!
        double norm;
        double x_f, y_f, z_f, phi_f, theta_f, psi_f;
        double qo_filt, qx_filt, qy_filt, qz_filt;
        double qo_dfilt, qx_dfilt, qy_dfilt, qz_dfilt;
        double x_dot, y_dot, z_dot, x_ddot, y_ddot, z_ddot;

        while( ros::ok() ) {

                //  Acquire the data...
                pBuff = buff;

                * ((int *) pBuff) = ClientCodes::EData;
                pBuff += sizeof(int);

                * ((int *) pBuff) = ClientCodes::ERequest;
                pBuff += sizeof(int);

                if( tcp.tcpSend(buff, pBuff - buff) <= 0 )
                        printf("\n \n467: Error Requesting Vehicle Position Data\n \n");

                int packet;
                int type;
                int size;

                //  Get and check the packet header...
                if( !tcp.tcpReceive(packet) )
                        printf("\n \n475: Error Recieving Vehicle Position Data\n \n");

                if( !tcp.tcpReceive(type) )
                        printf("\n \n478: Error Recieving Vehicle Position Data\n \n");

                if(type != ClientCodes::EReply) {
                        printf("\n \n481: Bad Type: %d\n \n", type);
                }

                if(packet != ClientCodes::EData) {
                        printf("\n \n485: Bad packet: %d\n\n", packet);
                }

                if( !tcp.tcpReceive(size) )
                        printf("\n \n489: Packet Size Wrong - Vehicle Position Data\n \n");

                if( size != infoSize ) {
                        printf("\n \n492: ERROR: stream size has changed to %d - should rescan channels for new names.\n", size);
                        return;
                }


                //  Get the data.
                std::vector< double >::iterator iData;

                for(iData = data.begin(); iData != data.end(); iData++) {
                        if( !tcp.tcpReceive(*iData) )
                                printf("\n \n502: Bad Data Packet - Vehicle Position Data\n \n");
                }

                //- Look Up Channels - - - - - - - - - - - - - - - - - - - - - - -
                //  Get the TimeStamp
                timestamp = data[FrameChannel];

                if(timestamp < timestamp_prev + 1.0) {
                        // added 8/16/2007 sahrens to sync packets with Vicon data
                        continue;
                }

                timestamp_prev = timestamp;


                //  Get the channels corresponding to the bodies.
                //=================================================================
                //  The bodies are in global space
                //  The world is Z-up
                //  The translational values are in millimeters
                //  The rotational values are in radians
                //=================================================================
                std::vector< BodyChannel >::iterator iBody;
                std::vector< BodyData >::iterator iBodyData;

                int body_number = 0;
                int next_heli_id = 10;
                int next_gv_id = 100;

                int SubjectIndex = -1;
                for(iBody = BodyChannels.begin(), iBodyData = bodyPositions.begin(); iBody != BodyChannels.end(); iBody++, iBodyData++) {
                        body_number++;
                        SubjectIndex++;

                        iBodyData->TX = data[iBody->TX]/1000.0;
                        iBodyData->TY = data[iBody->TY]/1000.0;
                        iBodyData->TZ = data[iBody->TZ]/1000.0;

                        //  The channel data is in the angle-axis form.
                        //  The following converts this to a quaternion.
                        norm = sqrt(data[iBody->RX] * data[iBody->RX] + data[iBody->RY] * data[iBody->RY] + data[iBody->RZ] * data[iBody->RZ]);

                        double ax, ay, az;
                        //normalize the axis angles
                        if (norm < 1e-10) {
                                ax	= data[iBody->RX];
                                ay	= data[iBody->RY];
                                az	= data[iBody->RZ];
                        } else {
                                ax	= data[iBody->RX]/norm;
                                ay	= data[iBody->RY]/norm;
                                az	= data[iBody->RZ]/norm;
                        }

                        double normOld = sqrt(data_prev[iBody->RX] * data_prev[iBody->RX] + data_prev[iBody->RY] * data_prev[iBody->RY] + data_prev[iBody->RZ] * data_prev[iBody->RZ]);

                        if (normOld < 1e-10) {
                                ax_prev	= data_prev[iBody->RX];
                                ay_prev	= data_prev[iBody->RY];
                                az_prev	= data_prev[iBody->RZ];
                        } else {
                                ax_prev = data_prev[iBody->RX]/norm;
                                ay_prev	= data_prev[iBody->RY]/norm;
                                az_prev	= data_prev[iBody->RZ]/norm;
                        }

                        double dotprod = ax*ax_prev + ay*ay_prev + az*az_prev;

                        // This check makes the quaternions continuous so the derivatives are smooth
                        //	Since the 2-norm of the angle axis vector is always unity a jump between
                        //    steps will indicate appx. -1.
                        if (dotprod < 0.0) {
                                flip = -flip;
                        }

                        double qo_cont, qx_cont, qy_cont, qz_cont;

                        // Switch from ENU to NED
                        qo_cont	=  flip*cos(norm/2.0);
                        qx_cont	=  flip*ay*sin(norm/2.0);	//ay switches with ax
                        qy_cont	=  flip*ax*sin(norm/2.0);
                        qz_cont	= -flip*az*sin(norm/2.0);	//az is opposite


                        // Calculate velocities, attitude rates, and accelerations

                        if(iBody->Name.find("Q") != std::string::npos) {
                                iBodyData->veh_id = next_heli_id;
                                next_heli_id++;
                        } else {
                                iBodyData->veh_id = next_gv_id;
                                next_gv_id++;
                        }

                        int loc = iBody->Name.find(":", 0);
                        std::string thisName = iBody->Name.substr(0, loc);

                        // bbethke, changed to generate the data string
                        // The current vehicle name is iBody->Name
                        // The current vehicle data is iBodyData->T[X,Y,Z], iBodyData->[pitch,roll,yaw]
                        // The current vehicle velocity is iBodyData->[x,y,z,t,r,w]dot
                        // just need to stick this all in a big string!



                        /******************  KALMAN FILTERING  *********************/
                        // added by sahrens 10/3/2007
                        z_K(1) = iBodyData->TX;
                        z_K(2) = iBodyData->TY;
                        z_K(3) = iBodyData->TZ;
                        z_K(4) = qo_cont;
                        z_K(5) = qx_cont;
                        z_K(6) = qy_cont;
                        z_K(7) = qz_cont;

                        // Run the filter!
                        iBodyData->filter.step(u_K,z_K);

                        // Display stuff
                        xest = iBodyData->filter.getX();
                        Pest = iBodyData->filter.calculateP();

                        //extract states
                        x_f       = xest(1);   // position
                        y_f       = xest(2);
                        z_f       = xest(3);
                        x_dot     = xest(4);   // velocity
                        y_dot     = xest(5);
                        z_dot     = xest(6);
                        x_ddot    = xest(7);   // acceleration
                        y_ddot    = xest(8);
                        z_ddot    = xest(9);
                        qo_filt   = xest(10);  //quaternion
                        qx_filt	= xest(11);
                        qy_filt	= xest(12);
                        qz_filt	= xest(13);
                        qo_dfilt	= xest(14);  //quaternion rate
                        qx_dfilt	= xest(15);
                        qy_dfilt  = xest(16);
                        qz_dfilt  = xest(17);

                        // renormalize - this should not be an issue but just in case the process noise covariance is not large enough
                        double norm_q		= sqrt(qo_filt*qo_filt + qx_filt*qx_filt + qy_filt*qy_filt + qz_filt*qz_filt);

                        qo_filt	= qo_filt/norm_q;
                        qx_filt	= qx_filt/norm_q;
                        qy_filt	= qy_filt/norm_q;
                        qz_filt	= qz_filt/norm_q;

                        /****************  END KALMAN FILTERING  *******************/





                        //Transformations are adopted from:
                        //Review of Attitude Representations Used for Aircraft Kinematics
                        //Journal of Aircraft Vol. 38, No.4, July-August 2001
                        // unless otherwise noted
                        phi_f		= atan2(2*(qo_filt*qx_filt + qy_filt*qz_filt),(qo_filt*qo_filt + qz_filt*qz_filt - qx_filt*qx_filt - qy_filt*qy_filt));
                        theta_f	= asin(2*(qo_filt*qy_filt - qx_filt*qz_filt));
                        psi_f		= atan2(2*(qo_filt*qz_filt + qx_filt*qy_filt),(qo_filt*qo_filt + qx_filt*qx_filt - qy_filt*qy_filt - qz_filt*qz_filt));

                        // convert quaternion/quaternion rates to body attitude rates
                        // Aircraft Control and Simulation - Stevens and Lewis
                        double p = 2*(qo_filt*qx_dfilt + qz_filt*qy_dfilt - qy_filt*qz_dfilt - qx_filt*qo_dfilt);
                        double q = 2*(-qz_filt*qx_dfilt + qo_filt*qy_dfilt + qx_filt*qz_dfilt - qy_filt*qo_dfilt);
                        double r = 2*(qy_filt*qx_dfilt - qx_filt*qy_dfilt + qo_filt*qz_dfilt - qz_filt*qo_dfilt);

                        //convert accel to body NED frame (exactly like an onboard accelerometer would generate)
                        //used matlab symbolic toolbox for analytical functions rather than importing a matrix/vector/rotation library
                        //previously: inv(euler2Rot(veh_state.att))*ENU2NED*(veh_state.acc + vec3(0.0, 0.0, GRAVITY))

                        //compute rotation matrix
                        //From room inertial to body
                        double r11 =  2*(qx_filt*qy_filt + qz_filt*qo_filt);
                        double r12 =  qx_filt*qx_filt + qo_filt*qo_filt - qy_filt*qy_filt - qz_filt*qz_filt;
                        double r13 =  -2*(qx_filt*qz_filt - qy_filt*qo_filt);

                        double r21 =  qy_filt*qy_filt + qo_filt*qo_filt - qx_filt*qx_filt - qz_filt*qz_filt;
                        double r22 =  2*(qx_filt*qy_filt - qz_filt*qo_filt);
                        double r23 =  -2*(qy_filt*qz_filt + qx_filt*qo_filt);

                        double r31 =  2*(qy_filt*qz_filt - qx_filt*qo_filt);
                        double r32 =  2*(qx_filt*qz_filt + qy_filt*qo_filt);
                        double r33 =  -(qz_filt*qz_filt + qo_filt*qo_filt - qx_filt*qx_filt - qy_filt*qy_filt);

        				// Populate ROS messages
        				geometry_msgs::PoseStamped POSE;
        				geometry_msgs::TwistStamped TWIST;
        				geometry_msgs::Vector3Stamped ACCEL;

        				POSE.header.stamp = ros::Time::now();
        				POSE.header.frame_id = thisName;
        				POSE.pose.position.x = y_f;			// ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
        				POSE.pose.position.y = -x_f;
        				POSE.pose.position.z = z_f;
        				POSE.pose.orientation.w = qo_filt;
        				POSE.pose.orientation.x = qx_filt;	// ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
        				POSE.pose.orientation.y = -qy_filt;
        				POSE.pose.orientation.z = -qz_filt;

        				TWIST.header.stamp = ros::Time::now();
        				TWIST.header.frame_id = thisName;
        				TWIST.twist.linear.x = y_dot;
        				TWIST.twist.linear.y = -x_dot;
        				TWIST.twist.linear.z = z_dot;
        				TWIST.twist.angular.x = p;
        				TWIST.twist.angular.y = -q;
        				TWIST.twist.angular.z = -r;

        				ACCEL.header.stamp = ros::Time::now();
        				ACCEL.header.frame_id = thisName;
        				ACCEL.vector.x = y_ddot;   // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
        				ACCEL.vector.y = -x_ddot;
        				ACCEL.vector.z = z_ddot;

        				// Broadcast ROS data
        				pose_pub[SubjectIndex].publish(POSE);
        				twist_pub[SubjectIndex].publish(TWIST);
        				accel_pub[SubjectIndex].publish(ACCEL);


//        				double roll, pitch, yaw;
//        				quaternion2Euler(POSE.pose.orientation, roll, pitch, yaw);
//        				ROS_INFO_STREAM_THROTTLE(1./5,"Roll: " << roll << " Pitch: " << pitch << " Yaw: " << yaw);



//			printf("%d %s %d %f %f %f %f %f %f %f\n",(int)timestamp, thisName.c_str(), iBodyData->veh_id, iBodyData->TX, iBodyData->TY, iBodyData->TZ, qx_cont, qy_cont, qz_cont, qo_cont);
//			UDPpacket_pos += sprintf_s(UDPpacket + UDPpacket_pos, UDPpacketSize, "%s %d,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f,%+f;",
//				thisName.c_str(), iBodyData->veh_id,
//				x_f, y_f, z_f,
//				phi_f, theta_f, psi_f,
//				x_dot, y_dot, z_dot,
//				p, q, r,
//				qy_cont, qx_cont, -qz_cont, qo_cont);  //qy_filt, qx_filt, -qz_filt, qo_filt);  //filtered quaternion is in NED - converting back to ENU for 3dvis compatibility


                        // log data
                        if( flag_logdata )
                                fprintf(fp_bodies,"%d %s %d %f %f %f %f %f %f %f\n",(int)timestamp, thisName.c_str(), iBodyData->veh_id, iBodyData->TX, iBodyData->TY, iBodyData->TZ, qx_cont, qy_cont, qz_cont, qo_cont);


                }  //only difference bewteen this and RAVEN_accel is the lack of acceleration in the output

                data_prev = data;


        }

}

