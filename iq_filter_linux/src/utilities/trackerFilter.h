#ifndef TRACKERFILTER_H
#define TRACKERFILTER_H

#include "../kalman/ekfilter.hpp"

#include <math.h>

#define POS_PROCESS_COV 30.0  // jerk - m/s^3
#define POS_MEASUREMENT_COV 4.0043e-8*100.0  // from matlab cov(truth.pos)
#define ATT_PROCESS_COV 1000.0*M_PI/180.0/3.0  // angular accel - rad/s^2
#define ATT_MEASUREMENT_COV 6.315e-7*100.0  // from matlab cov(truth.att)
#define M_PI 3.14159265

class CTrackerFilter : public Kalman::EKFilter<double,1> {

public:
        CTrackerFilter();

protected:

        void makeBaseA();
        void makeBaseH();
        void makeBaseV();
        void makeBaseR();
        void makeBaseW();
        void makeBaseQ();
        void makeProcess();
        void makeMeasure();

        double deltaT;
};

// Convenience definitions
typedef CTrackerFilter::Vector Vector;
typedef CTrackerFilter::Matrix Matrix;

#endif
