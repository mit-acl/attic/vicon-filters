/**
 * main.cpp
 * Implements a simple g-h-k filter for the vicon data.  Outputs filtered data
 * as ROS messages.
 *
 *  Created on: Sep 23, 2013
 *      Author: mark
 */

#include <signal.h>

#include <vicon.h>

// ROS Includes
#include <ros/ros.h> // standard ros header

int main(int argc, char* argv[])
{

// Initialize ROS
	ros::init(argc, argv, "vicon");

	// Initialize broadcast node for sending the data out
	ros::NodeHandle nh;

	// Make a new vicon client
	Vicon v;
		
	while (ros::ok())
	{
		v.runFilter();
		v.screenPrint();
	}
	// TODO: a service to allow resetting.
}
