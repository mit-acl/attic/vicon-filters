#ifndef TIMER_HPP_
#define TIMER_HPP_

#include <sys/time.h>
#include <sys/select.h>
#include <semaphore.h>
#include <iostream>

class Timer
{
private:
        double _Ts; // Timestep, in SECONDS
        sem_t sem_timer; // Semaphore for timer control

        // Timer Variables
        struct timespec timestops;
        struct timespec timestopd;

        int count_me;

public:

        // Initializes the timer, input is the desired timestep in SECONDS
        void init(double dt);

        void wait();
};

#endif /*TIMER_HPP_*/
