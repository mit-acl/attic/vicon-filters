/*!
 * \file body.h
 *
 * Class holding position and orientation filters for vicon objects.
 *
 * Created on: Oct 16, 2013
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#ifndef BODY_H_
#define BODY_H_

#include <ros/ros.h> // standard ros header
#include <geometry_msgs/Pose.h>  // for the position and orientation
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PoseStamped.h>  // for the position and orientation
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Quaternion.h>
#include <tracker_filter_linux/ViconRaw.h>
#include <eigen3/Eigen/Dense>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <deque>
#include <algorithm> 

#include "GHKFilter.h"
#include "attitude_ekf.h"

/**
 *  This class has position and orientation filters as members.
 */
class Body
{
public:
	Body();
	~Body();

	void update(double dt, geometry_msgs::Pose meas, bool occ, unsigned int marker_count, unsigned int num_of_markers);
	void broadcastData();

	void initPublishers();
	void resetStates();

	geometry_msgs::Pose getPose() { return pose; }
	geometry_msgs::Twist getTwist() { return twist; }
	geometry_msgs::Vector3 getAccel() { return accel; }
	geometry_msgs::Pose getMeasurement() { return measurement_flipped;}
	std_msgs::Bool isOccluded() { return occluded;}

	std::string getName() { return name; }
	void setName(std::string name) { this->name = name; }
	int prop_counter_pos;
	int prop_counter_att;

	bool publisher_initialized;

protected:
	void propagationUpdate(double dt);
	void measurementUpdate(double dt, geometry_msgs::Pose meas, bool occ, unsigned int marker_count, unsigned int num_of_markers);
	ros::Publisher pose_pub;
	ros::Publisher twist_pub;
	ros::Publisher accel_pub;

	ros::Publisher measure_pub;
	ros::Publisher occluded_pub;
	ros::Publisher vicon_raw_pub;

	// Tuning Parameters
	double distance_threshold;
	double rotation_threshold;
	bool flip_coordinate_system;
	int log_length;
	int cont_flag_threshold_pos;
	int cont_flag_threshold_att;
	int cont_flag_threshold_marker;
	int missing_marker_threshold;
	int max_prop_steps_pos;
	int max_prop_steps_att;


	// ros::Publisher diff_pub;

	std::string name; ///< Body name as specified in Tracker software
	
	geometry_msgs::Pose pose; ///< Body pose (position + orientation)
	geometry_msgs::Twist twist; ///< Body linear and angular velocities
	geometry_msgs::Vector3 accel; ///< Body linear acceleration
	geometry_msgs::Pose measurement_flipped; ///< Body pose (position + orientation)
	std_msgs::Bool occluded;
	tracker_filter_linux::ViconRaw vicon_raw;
	
	geometry_msgs::Point last_good_pos;
	geometry_msgs::Quaternion last_good_att;

	std::deque<geometry_msgs::Pose> measurement_log; //First being the most recent
	std::deque<bool> no_jump_pos_log;
	std::deque<bool> no_jump_att_log; 
	std::deque<bool> full_marker_log; 

	GHKFilter ghk; ///< position, velocity, acceleration filter
	AttitudeEKF ekf; ///< attitude and attitude rate filter

	geometry_msgs::Quaternion getFlipProcessedQuaternion(geometry_msgs::Quaternion q_ref,
		geometry_msgs::Quaternion q_in);
	double getEuclideanDistanceSq(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b);
	double getEuclideanDistanceSq(geometry_msgs::Point a, geometry_msgs::Point b);
	double getAngleDifference(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b);
	void log_measurement(geometry_msgs::Pose measurement, bool no_jump_pos, bool no_jump_att, bool full_marker_flag);
	bool isAllTrue(std::deque<bool> list, int max_count);
	bool isAllTrue(std::deque<bool> list);

};

#endif /* BODY_H_ */
