#include "requestChannelInfo.hpp"

int requestChannelInfo(TCPPort &tcp, std::vector<MarkerChannel> &MarkerChannels, std::vector<BodyChannel> &BodyChannels, int &FrameChannel)
{
        std::vector< std::string > info;
        const int bufferSize = 50000;
        char buff[bufferSize];
        char *pBuff;

        //- Get Info - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  Request the channel information
        pBuff = buff;

        * ((int *) pBuff) = ClientCodes::EInfo;
        pBuff += sizeof(int);

        * ((int *) pBuff) = ClientCodes::ERequest;
        pBuff += sizeof(int);

        if( tcp.tcpSend(buff,pBuff-buff) <= 0 )
                printf("\n \n275: Error Requesting Packet \n \n");


        int packet;
        int type;

        if( !tcp.tcpReceive(packet) )
                printf("\n \n282: Error Receiving Packet \n \n");

        if( !tcp.tcpReceive(type) )
                printf("\n \n285: Error Receiving Packet \n \n");

        if(type != ClientCodes::EReply)
                printf("\n \n289: Bad Packet\n \n");

        if(packet != ClientCodes::EInfo) {
                printf("\n \n292: Bad Reply Type, exiting\n \n");
                return(0);
        }

        int size;
        if( !tcp.tcpReceive(size) )
                printf("\n \n298: Something wrong with size of packet received...\n \n");

        info.resize(size);
        std::vector< std::string >::iterator iInfo;

        for(iInfo = info.begin(); iInfo != info.end(); iInfo++) {
                int s;
                char c[255];
                char * p = c;

                if( !tcp.tcpReceive(s) )
                        printf("\n \n312: Something wrong with size of packet received...\n \n");

                if( !tcp.tcpReceive(c,s) )
                        printf("\n \n315: Something wrong with size of packet received...\n \n");

                p += s;

                *p = 0;

                *iInfo = std::string(c);
        }


        //- Parse Info - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  The info packets now contain the channel names.
        //  Identify the channels with the various dof's.
        for(iInfo = info.begin(); iInfo != info.end(); iInfo++) {
                //  Extract the channel type

                int openBrace = iInfo->find('<');

                if(openBrace == iInfo->npos)
                        printf("\n \n340: Bad Channel Id\n \n");

                int closeBrace = iInfo->find('>');

                if(closeBrace == iInfo->npos)
                        printf("\n \n345: Bad Channel Id\n \n");

                closeBrace++;

                std::string Type = iInfo->substr(openBrace, closeBrace-openBrace);

                //  Extract the Name
                std::string Name = iInfo->substr(0, openBrace);
                printf("Found NAME: %s\n",Name.c_str());

                int space = Name.rfind(' ');
                if(space != Name.npos)
                        Name.resize(space);

                std::vector< MarkerChannel >::iterator iMarker;
                std::vector< BodyChannel >::iterator iBody;
                std::vector< std::string >::const_iterator iTypes;
                iMarker = std::find( MarkerChannels.begin(), MarkerChannels.end(), Name );
                iBody = std::find(BodyChannels.begin(), BodyChannels.end(), Name);

                if(iMarker != MarkerChannels.end()) {
                        //  The channel is for a marker we already have.
                        iTypes = std::find(ClientCodes::MarkerTokens.begin(), ClientCodes::MarkerTokens.end(), Type);

                        if(iTypes != ClientCodes::MarkerTokens.end())
                                iMarker->operator[](iTypes - ClientCodes::MarkerTokens.begin()) = iInfo - info.begin();

                } else if(iBody != BodyChannels.end()) {
                        //  The channel is for a body we already have.
                        iTypes = std::find(ClientCodes::BodyTokens.begin(), ClientCodes::BodyTokens.end(), Type);

                        if(iTypes != ClientCodes::BodyTokens.end())
                                iBody->operator[](iTypes - ClientCodes::BodyTokens.begin()) = iInfo - info.begin();

                } else if((iTypes = std::find(ClientCodes::MarkerTokens.begin(), ClientCodes::MarkerTokens.end(), Type)) != ClientCodes::MarkerTokens.end()) {
                        //  Its a new marker.
                        MarkerChannels.push_back(MarkerChannel(Name));
                        MarkerChannels.back()[iTypes - ClientCodes::MarkerTokens.begin()] = iInfo - info.begin();

                } else if((iTypes = std::find(ClientCodes::BodyTokens.begin(), ClientCodes::BodyTokens.end(), Type)) != ClientCodes::BodyTokens.end()) {
                        //  Its a new body.
                        BodyChannels.push_back(BodyChannel(Name));
                        BodyChannels.back()[iTypes - ClientCodes::BodyTokens.begin()] = iInfo - info.begin();

                } else if(Type == "<F>") {
                        FrameChannel = iInfo - info.begin();

                } else {
                        //      It could be a new channel type.
                }
        }

        return(info.size());

}
