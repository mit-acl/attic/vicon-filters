#ifndef RELAYBODYDATA_HPP_
#define RELAYBODYDATA_HPP_

#include "utilities/tcpPort.hpp"
#include "utilities/viconClasses.hpp"
#include "kalman/kvector.hpp"
#include "kalman/kmatrix.hpp"

// ROS Includes
#include <ros/ros.h> // standard ros header
#include <geometry_msgs/PoseStamped.h>  // for the position and orientation
#include <geometry_msgs/TwistStamped.h> // for the velocity and angular velocities
#include <geometry_msgs/Vector3Stamped.h> // for the acceleration

using namespace Kalman;

#define PI 3.14159

void quaternion2Euler(geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw);
void relayBodyData(TCPPort &tcp, std::vector<MarkerChannel> &MarkerChannels,
                   std::vector<BodyChannel> &BodyChannels, int &FrameChannel, int infoSize,
                   bool flag_dcm, bool flag_udp, bool flag_logdata);

#endif /*RELAYBODYDATA_HPP_*/
