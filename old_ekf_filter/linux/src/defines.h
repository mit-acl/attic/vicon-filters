
#include "kalman/kvector.hpp"
#include "kalman/kmatrix.hpp"
#include "kalman/trackerFilter.h"

typedef class Body{
	public:
	Body()
	{
		x_prev=0;
		y_prev=0;
		z_prev=0;
		q0_prev=0;
		qx_prev=0;
		qy_prev=0;
		qz_prev=0;
	}
	char name[50];
	int id;			
	double x;
	double y;	
	double z;
	double x_prev;
	double y_prev;	
	double z_prev;
	double q0;
	double qx;
	double qy;
	double qz;
	double q0_prev;
	double qx_prev;
	double qy_prev;
	double qz_prev;
	
	CTrackerFilter filter;	
};