/*!
 * \file body.cpp
 *
 * @todo Brief file description
 *
 * Created on: Oct 16, 2013
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#include "body.h"

Body::Body(){
	prop_counter_pos = 0;
	prop_counter_att = 0;

	distance_threshold = 0.22;
	rotation_threshold = 0.1;
	log_length = 30;
	missing_marker_threshold = 1;
	max_prop_steps_pos = 5;
	max_prop_steps_att = 10;

	cont_flag_threshold_pos = 10;
	cont_flag_threshold_att = 25;
	cont_flag_threshold_marker = 3;

	flip_coordinate_system = true;
	publisher_initialized = false;

	// Read parameters if available.
	if(ros::param::has("~distance_threshold")){ros::param::getCached("~distance_threshold",distance_threshold);}
	if(ros::param::has("~rotation_threshold")){ros::param::getCached("~rotation_threshold",rotation_threshold);}
	if(ros::param::has("~log_length")){ros::param::getCached("~log_length",log_length);}
	if(ros::param::has("~missing_marker_threshold")){ros::param::getCached("~missing_marker_threshold",missing_marker_threshold);}
	if(ros::param::has("~max_prop_steps_pos")){ros::param::getCached("~max_prop_steps_pos",max_prop_steps_pos);}
	if(ros::param::has("~max_prop_steps_att")){ros::param::getCached("~max_prop_steps_att",max_prop_steps_att);}
	if(ros::param::has("~cont_flag_threshold_pos")){ros::param::getCached("~cont_flag_threshold_pos",cont_flag_threshold_pos);}
	if(ros::param::has("~cont_flag_threshold_att")){ros::param::getCached("~cont_flag_threshold_att",cont_flag_threshold_att);}
	if(ros::param::has("~cont_flag_threshold_marker")){ros::param::getCached("~cont_flag_threshold_marker",cont_flag_threshold_marker);}
	if(ros::param::has("~flip_coordinate_system")){ros::param::getCached("~flip_coordinate_system",flip_coordinate_system);}
}

Body::~Body(){
	// shut down the publishers
	pose_pub.shutdown();
	twist_pub.shutdown();
	accel_pub.shutdown();
	measure_pub.shutdown();
	vicon_raw_pub.shutdown();
}

void Body::initPublishers()
{
	std::string posename = name  + "/pose";
	std::string velname = name  + "/vel";
	std::string accelname = name  + "/accel";
	std::string measurename = name  + "/measurement";
	std::string viconrawname = name  + "/viconraw";

	ROS_INFO_STREAM(
			"Broadcasting data for: " << posename << ", "
			<< velname << ", " << accelname
			<< measurename << ", " << viconrawname 
			<< std::endl);

	ros::NodeHandle nh;
	pose_pub = nh.advertise<geometry_msgs::PoseStamped>(posename, 1);
	twist_pub = nh.advertise<geometry_msgs::TwistStamped>(velname, 1);
	accel_pub = nh.advertise<geometry_msgs::Vector3Stamped>(accelname, 1);
	measure_pub = nh.advertise<geometry_msgs::PoseStamped>(measurename,1);
	vicon_raw_pub = nh.advertise<tracker_filter_linux::ViconRaw>(viconrawname,1);

	publisher_initialized = true;
}

void Body::broadcastData()
{
	if (publisher_initialized){
		// Populate ROS messages
		geometry_msgs::PoseStamped p;
		geometry_msgs::TwistStamped t;
		geometry_msgs::Vector3Stamped a;
		tracker_filter_linux::ViconRaw v;
		geometry_msgs::PoseStamped m;

		ros::Time tn = ros::Time().now();
		p.header.stamp = tn;
		p.header.frame_id = "vicon";
		/// \todo change vicon coordinates!
		
		if (flip_coordinate_system){
			p.pose.position.x = pose.position.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			p.pose.position.y = -pose.position.x;
			p.pose.position.z = pose.position.z;
			p.pose.orientation.w = pose.orientation.w;
			p.pose.orientation.x = pose.orientation.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			p.pose.orientation.y = -pose.orientation.x;
			p.pose.orientation.z = pose.orientation.z;
		}
		else{
			p.pose.position.x = pose.position.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			p.pose.position.y = pose.position.y;
			p.pose.position.z = pose.position.z;
			p.pose.orientation.w = pose.orientation.w;
			p.pose.orientation.x = pose.orientation.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			p.pose.orientation.y = pose.orientation.y;
			p.pose.orientation.z = pose.orientation.z;			
		}

		m.header.stamp = tn;
		m.header.frame_id = "vicon";
		if (flip_coordinate_system){
			m.pose.position.x = measurement_flipped.position.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			m.pose.position.y = -measurement_flipped.position.x;
			m.pose.position.z = measurement_flipped.position.z;
			m.pose.orientation.w = measurement_flipped.orientation.w;
			m.pose.orientation.x = measurement_flipped.orientation.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			m.pose.orientation.y = -measurement_flipped.orientation.x;
			m.pose.orientation.z = measurement_flipped.orientation.z;
		}
		else{
			m.pose.position.x = measurement_flipped.position.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			m.pose.position.y = measurement_flipped.position.y;
			m.pose.position.z = measurement_flipped.position.z;
			m.pose.orientation.w = measurement_flipped.orientation.w;
			m.pose.orientation.x = measurement_flipped.orientation.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			m.pose.orientation.y = measurement_flipped.orientation.y;
			m.pose.orientation.z = measurement_flipped.orientation.z;			
		}


		t.header.stamp = tn;
		t.header.frame_id = "vicon";
		
		if (flip_coordinate_system){
			t.twist.linear.x = twist.linear.y;
			t.twist.linear.y = -twist.linear.x;
			t.twist.linear.z = twist.linear.z;
			t.twist.angular.x = twist.angular.y;
			t.twist.angular.y = -twist.angular.x;
			t.twist.angular.z = twist.angular.z;
		}
		else{
			t.twist.linear.x = twist.linear.x;
			t.twist.linear.y = -twist.linear.y;
			t.twist.linear.z = twist.linear.z;
			t.twist.angular.x = twist.angular.x;
			t.twist.angular.y = twist.angular.y;
			t.twist.angular.z = twist.angular.z;			
		}

		a.header.stamp = tn;
		a.header.frame_id = "vicon";
		if (flip_coordinate_system){
			a.vector.x = accel.y; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			a.vector.y = -accel.x;
			a.vector.z = accel.z;
		}
		else{
			a.vector.x = accel.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
			a.vector.y = accel.y;
			a.vector.z = accel.z;			
		}

		// Fill vicon_raw for publishment, No flip.
		v.measurement.position.x = vicon_raw.measurement.position.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		v.measurement.position.y = vicon_raw.measurement.position.y;
		v.measurement.position.z = vicon_raw.measurement.position.z;
		v.measurement.orientation.w = vicon_raw.measurement.orientation.w;
		v.measurement.orientation.x = vicon_raw.measurement.orientation.x; // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
		v.measurement.orientation.y = vicon_raw.measurement.orientation.y;
		v.measurement.orientation.z = vicon_raw.measurement.orientation.z;

		v.occluded = vicon_raw.occluded;
		v.marker_count = vicon_raw.marker_count;
		v.num_of_markers = vicon_raw.num_of_markers;

		// Broadcast ROS data
		pose_pub.publish(p);
		twist_pub.publish(t);
		accel_pub.publish(a);
		measure_pub.publish(m);
		vicon_raw_pub.publish(v);
	}

}

void Body::resetStates()
{
	// Set initial state estimates
	pose.position.x = pose.position.y = pose.position.z = 0.0;
	pose.orientation.w = 1.0;
	pose.orientation.x = pose.orientation.y = pose.orientation.z = 0.0;
	twist.linear.x = twist.linear.y = twist.linear.z = 0.0;
	twist.angular.x = twist.angular.y = twist.angular.z = 0.0;
	accel.x = accel.y = accel.z = 0.0;

	measurement_flipped = pose;
	vicon_raw.measurement = pose;
	vicon_raw.occluded.data = true;
	measurement_log.clear();
	no_jump_pos_log.clear();
	no_jump_att_log.clear();
	full_marker_log.clear();

	occluded.data = true;
	publisher_initialized = false;
}

void Body::propagationUpdate(double dt)
{
	prop_counter_pos+=1;
	prop_counter_att+=1;
	if (prop_counter_pos < max_prop_steps_pos){
		ghk.propUpdate(dt);
	}
	if (prop_counter_att < max_prop_steps_att){
		ekf.propUpdate(dt);
	}
}

void Body::update(double dt, geometry_msgs::Pose meas, bool occ, unsigned int marker_count, unsigned int num_of_markers){
	if (not occ){
		// Initalize the publisher only when the first unoccluded measurement is receieved.
		if (not publisher_initialized){
			initPublishers();
		}
	}

	propagationUpdate(dt);
	measurementUpdate(dt, meas, occ, marker_count, num_of_markers);
}

void Body::measurementUpdate(double dt, geometry_msgs::Pose measurement, bool occluded_flag,
	unsigned int marker_count, unsigned int num_of_markers)
{
	// Save the raw measurement (Pose) and occluded flag for publishing
	occluded.data = occluded_flag; //TODO: remove occluded, use vicon_raw instead
	vicon_raw.measurement = measurement;
	vicon_raw.occluded.data = occluded_flag;
	vicon_raw.marker_count.data = marker_count;
	vicon_raw.num_of_markers.data = num_of_markers;
	bool enough_markers = false;
	if (num_of_markers - marker_count <= missing_marker_threshold){
		enough_markers = true;
	}

	if ((not occluded_flag) && enough_markers){
		// Initialization for the first measurement
		if (measurement_log.size() == 0){
			// Set as last_good_pos and att.
			last_good_pos = measurement.position;
			last_good_att = measurement.orientation;
			// Add artifical log of the same
			log_measurement(measurement,true,true,true);
		}
		// Flip the quaterion of the measurement to be consistent with the last good att.
		// measurement.orientation = getFlipProcessedQuaternion(last_good_att,measurement.orientation);
		measurement.orientation = getFlipProcessedQuaternion(ekf.getAtt(),measurement.orientation);
		measurement_flipped = measurement; 

		// Check for jumps in pos and att against the previous (none occluded) measurement
		geometry_msgs::Pose measurement_prev = measurement_log.front();

		bool no_jump_pos = getEuclideanDistanceSq(measurement_prev.position,measurement.position) < distance_threshold*distance_threshold; //TODO: find good threashold
		bool no_jump_att = getAngleDifference(measurement_prev.orientation,measurement.orientation) < rotation_threshold; //TODO: find good threashold
		bool full_marker_flag = false;
		if (num_of_markers == marker_count){
			full_marker_flag = true;
		}
		log_measurement(measurement,no_jump_pos,no_jump_att,full_marker_flag);

		// Handle positoin measurement update
		if (isAllTrue(no_jump_pos_log,cont_flag_threshold_pos)){
			// Consistent measurement without jumps, definitely use the current one
			ghk.measUpdate(dt,measurement.position);
			prop_counter_pos = 0;
			// Also set the last_good_pos
			last_good_pos = measurement.position;
		}
		else{
			// Some jumps in the log, do measurement update only if close to current pos estimation.
			if (getEuclideanDistanceSq(ghk.getPos(),measurement.position) < distance_threshold*distance_threshold){
				ghk.measUpdate(dt,measurement.position);
				prop_counter_pos = 0;
			}
			else{
				// prop_counter_pos+=1;
			}
		}

		// Handle attitude measurement update
		if (isAllTrue(no_jump_att_log,cont_flag_threshold_att) || isAllTrue(full_marker_log,cont_flag_threshold_marker)){
			ekf.measUpdate(measurement.orientation);
			prop_counter_att = 0;
			last_good_att = measurement.orientation;
		}
		else{
			if (getAngleDifference(ekf.getAtt(),measurement.orientation) < rotation_threshold){
				ekf.measUpdate(measurement.orientation);
				prop_counter_att = 0;
			}
			else{
				// prop_counter_att+=1;
			}
		}
	}
	else{ // occluded

	}

	// Extract state.
	pose.position = ghk.getPos();
	twist.linear = ghk.getVel();
	accel = ghk.getAcc();
	pose.orientation = ekf.getAtt();
	twist.angular = ekf.getRate();


	// Check for nan
}

void Body::log_measurement(geometry_msgs::Pose measurement, bool no_jump_pos, bool no_jump_att, bool full_marker_flag)
{
	// Push in the latest measurement and flags
	measurement_log.push_front(measurement);
	no_jump_pos_log.push_front(no_jump_pos);
	no_jump_att_log.push_front(no_jump_att);
	full_marker_log.push_front(full_marker_flag);

	// Drop the oldest measurement
	if(measurement_log.size() > 50)
	{
		measurement_log.pop_back();
		no_jump_pos_log.pop_back();
		no_jump_att_log.pop_back();
		full_marker_log.pop_back();
	}
}

geometry_msgs::Quaternion Body::getFlipProcessedQuaternion(geometry_msgs::Quaternion q_ref,
	geometry_msgs::Quaternion q_in)
{
	// Return a flipped(or non-flipped) version of q_in that is consistent with q_ref.
	// Compute the squared Euclidean distance between the reference quaternion and the flipped
	// and non-flipped version of the input quaternion
	double dist_sq = getEuclideanDistanceSq(q_ref,q_in);
	
	geometry_msgs::Quaternion q_flip = q_in;
	q_flip.w = -q_in.w;
	q_flip.x = -q_in.x;
	q_flip.y = -q_in.y;
	q_flip.z = -q_in.z;

	double dist_sq_flipped = getEuclideanDistanceSq(q_ref,q_flip);
	if (dist_sq_flipped < dist_sq){
		return q_flip;
	}
	else{
		return q_in;
	}
}


// Return the square of the distance between point a and b
double Body::getEuclideanDistanceSq(geometry_msgs::Point a, geometry_msgs::Point b)
{
	return (a.x - b.x)*(a.x - b.x)+ (a.y - b.y)*(a.y - b.y)+ (a.z - b.z)*(a.z - b.z);
}

double Body::getEuclideanDistanceSq(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b)
{
	return 	(a.w - b.w)*(a.w - b.w) + (a.x - b.x)*(a.x - b.x) +
					(a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z);
}

double Body::getAngleDifference(geometry_msgs::Quaternion a, geometry_msgs::Quaternion b)
{
	Eigen::Quaterniond q_a(a.w, a.x, a.y, a.z);
	Eigen::Quaterniond q_b(b.w, b.x, b.y, b.z);
	return q_a.angularDistance(q_b);
}

bool Body::isAllTrue(std::deque<bool> list, int max_count){
// Return if the first # of max_count flags are true. Return true if list is empty.
	for (int i = 0; i < std::min(int(list.size()),max_count); i++){
		if (list[i] == false){
			return false;
		}
	}
	return true;	
}
bool Body::isAllTrue(std::deque<bool> list){
	return isAllTrue(list,list.size());
}
