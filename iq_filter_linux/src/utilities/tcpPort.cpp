#include "tcpPort.hpp"


//int TCPPort::initWinsock()
//{
//    WORD      wVersionRequested;
//    WSADATA   wsaData;
//
//    wVersionRequested = MAKEWORD(1,1);
//    if (WSAStartup(wVersionRequested, &wsaData) != 0)
//    {
//        //cerr << " Winsock init failed." << endl;
//        return 1;
//    }
//    return 0;
//}


int TCPPort::tcpInit(std::string addr, int inPort)
{
		//initWinsock();
        // inputs
        port = inPort;

        // socket
        sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if(sock < 0) {
                printf("\n\nERROR:: TCP Socket Creation Error on %s, %d\n\n",addr.c_str(),inPort);
                return(-1);
        }

        // port
        //bzero(&address, sizeof(address));
		memset(&address, 0, sizeof(address));
        address.sin_family = AF_INET;
        address.sin_port   = htons(port);
        address.sin_addr.s_addr = inet_addr(addr.c_str());

        // connect
        if( connect( sock, (struct sockaddr*) &address, sizeof(address)) != 0 ) {
                printf("\n\nERROR:: TCP Failed to Connect to Socket on %s, %d\n\n",addr.c_str(),inPort);
                return(-1);
        }

        printf("TCP connected to '%s' on port %d\n",addr.c_str(),port);
        return(1);
}

void TCPPort::tcpClose()
{
        printf("Closing TCP port %d\n",port);
        //closesocket(sock);
}

int TCPPort::tcpSend(char* pkt, int sz)
{
        return send(sock, pkt, sz, 0);
}

int TCPPort::tcpSend(char* pkt)
{
        return send(sock, pkt, strlen(pkt), 0);
}


int TCPPort::tcpReceiveI(char* str, int sz)
{
        return recv(sock, str, sz, 0);
}

int TCPPort::tcpReceiveI(char* str)
{
        return recv(sock, str, strlen(str), 0);
}


bool TCPPort::tcpReceive(char *pBuffer, int BufferSize)
{
        char *p = pBuffer;
        char *e = pBuffer + BufferSize;

        int result;

        while(p != e)  // less than?
        {
                result = recv( sock, p, e - p, 0 );

                if (result < 0)
                        return false;

                p += result;
        }

        return true;
}

bool TCPPort::tcpReceive(int &Val)
{
        return tcpReceive((char*) &Val, sizeof(Val));
}

bool TCPPort::tcpReceive(unsigned int &Val)
{
        return tcpReceive((char*) &Val, sizeof(Val));
}

bool TCPPort::tcpReceive(double &Val)
{
        return tcpReceive((char*) &Val, sizeof(Val));
}
