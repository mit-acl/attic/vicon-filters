#ifndef REQUESTCHANNELINFO_HPP_
#define REQUESTCHANNELINFO_HPP_

#include "utilities/tcpPort.hpp"
#include "utilities/viconClasses.hpp"

int requestChannelInfo(TCPPort &tcp, std::vector<MarkerChannel> &M, std::vector<BodyChannel> &B, int &F);

#endif /*REQUESTCHANNELINFO_HPP_*/
