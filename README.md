# How to run the new vicon filters

* Turn on Vicon
* Start Vicon tracker on `armstrong`
* Execute the following command on `sikorsky`, or any machine with the tracker_filter installed.
```
$ roslaunch tracker_filter_linux tracker.launch
```
* Note that you should _NOT_ run the `ROSTracker` on `armstrong` anymore.

# Vicon Filters

This package contains all of the current filters used for getting the raw vicon data into a usable format for control.  At a high level this consists of differentiating and smoothing the raw position and orientation measurements that come from Vicon and piping that data out over ROS.

Note: these packages all explicitly assume that the Vicon machine can be accessed at 192.168.0.9:801.

### tracker_filter_linux

This can be run on any linux machine that is on the same local network as the Vicon machine.  It filters the position data using a simple GHK filter and the orientation data using an EKF.  This is the main filter used by the lab.

### iq_filter_linux

This filter uses the old IQ software that runs the old set of cameras for the lab.  This was last used 12/2013 to fly a quadrotor in the wind tunnel.  I'm relatively certain that it works (things flew well), but I'm not sure how robust it is.  It's only been tested in ideal circumstances.

### old_ekf_filter

This contains Windows and linux versions of the old filter used by the lab.  This filter combined position and attitude data together in a giant 17-state EKF.  The code was not particularly well documented.  However, it was used by the lab for many years and so should work.  However, the linux implementation of this filter is purely experimental.