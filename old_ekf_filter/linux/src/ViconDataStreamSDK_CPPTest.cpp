///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) OMG Plc 2009.
// All rights reserved.  This software is protected by copyright
// law and international treaties.  No part of this software / document
// may be reproduced or distributed in any form or by any means,
// whether transiently or incidentally to some other use of this software,
// without the written permission of the copyright owner.
//
///////////////////////////////////////////////////////////////////////////////

// NOTE - Mark Cutler, 3/20/2012
// To get the project to run in release mode with ROS I had to change the following property in the solution:
// Configuration Properties -> C/C++ -> Code Generation -> Runtime Library = Multi-threaded Debug DLL (/MDd)

#include <Client.h>
#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <signal.h>

#include <fstream>

#include "defines.h"

// ROS Includes
#include <ros/ros.h> // standard ros header
#include <std_msgs/String.h>  // for the message that contains everything being broadcast
#include <geometry_msgs/PoseStamped.h>  // for the position and orientation
#include <geometry_msgs/TwistStamped.h> // for the velocity and angular velocities
#include <geometry_msgs/Vector3Stamped.h> // for the acceleration

using namespace ViconDataStreamSDK::CPP;

// Global vars
bool ABORT = false;

// Prototypes
void controlC(int sig);

int main( int argc, char* argv[] )
{

	std::ofstream logFile;
	logFile.open("viconLog.txt");

	// Initialize ROS
	ros::init(argc,argv,"ViconBroadcastLinux2", ros::init_options::NoSigintHandler);

	//## Setup the CTRL-C trap
	signal(SIGINT, controlC);

	// Initialize broadcast node for sending the data out
	ros::NodeHandle broadcast_node;

	// Make a new client
	Client MyClient;

	// Connect to a server
	std::cout << "Connecting..." << std::flush;
	while( !MyClient.IsConnected().Connected )
	{
		// Direct connection
		MyClient.Connect( "192.168.0.9:801" );

		// Multicast connection
		// Output_ConnectToMulticast _Output_ConnectToMulticast = MyClient.ConnectToMulticast( "localhost:801", "224.0.0.0" );

		std::cout << ".";

	}
	std::cout << std::endl;

	// Enable some different data types
	MyClient.EnableSegmentData();
	MyClient.EnableMarkerData();
	MyClient.EnableUnlabeledMarkerData();
	MyClient.EnableDeviceData();

	// Set the streaming mode
	//MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPull );
	// MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPullPreFetch );
	MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ServerPush );

	// Set the global up axis
	MyClient.SetAxisMapping(
		Direction::Forward, 
		Direction::Left, 
		Direction::Up ); // Z-up

	std::vector< double > flip;

	// Initialize vector of ROS publishers
	std::vector<ros::Publisher> pose_pub;
	std::vector<ros::Publisher> twist_pub;
	std::vector<ros::Publisher> accel_pub;
	ros::Publisher names_pub;

	while(not ABORT)
	{
		//Initial filter setup
		while( MyClient.GetFrame().Result != Result::Success )
		{}

		//## Variables for tracked bodies
		unsigned int nBodies = MyClient.GetSubjectCount().SubjectCount;		
		std::vector< Body > Bodies;
		Bodies.resize(nBodies);
		flip.resize(nBodies);

		// Resize ROS publishers
		pose_pub.resize(nBodies);
		twist_pub.resize(nBodies);
		accel_pub.resize(nBodies);

		/******************  KALMAN FILTER SETUP  *********************/
		// added by sahrens 10/3/2007 (adapted from bbethke's vision_estimation project)
		printf("Initializing Kalman filter...\n");
		Kalman::selectKVectorContext(Kalman::createKVectorContext(" ", "[ ", " ];", 4));
		Kalman::selectKMatrixContext(Kalman::createKMatrixContext(" ", " ;\n  ", "[ ", " ];", 4));

		std_msgs::String viconData;

		// Initial state covariance matrix and ROS topics
		int SubjectNum = -1;
		std::vector< Body >::iterator iBody;
		for(iBody = Bodies.begin(); iBody != Bodies.end(); iBody++) {
			SubjectNum++;
			double p = 1;
			static const double P0_[] = {
				p,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,p,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,p,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,p,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,p,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,p,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,p,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,p,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,p,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,p,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,p,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,p,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,p,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,p,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,p,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,p,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,p,
			};
			Matrix P0(17, 17, P0_);

			// Initial state estimate
			Vector x(17);
			x(1)  = 0;  // x (m)
			x(2)  = 0;  // y (m)
			x(3)  = 0;  // z (m)
			x(4)  = 0;  // xdot (m/s)
			x(5)  = 0;  // ydot (m/s)
			x(6)  = 0;  // zdot (m/s)
			x(7)  = 0;  // xddot (m/s^2)
			x(8)  = 0;  // yddot (m/s^2)
			x(9)  = 0;  // zddot (m/s^2)
			x(10) = 1;  // qo
			x(11) = 0;  // qx
			x(12) = 0;  // qy
			x(13) = 0;  // qz
			x(14) = 0;  // qo_dot
			x(15) = 0;  // qx_dot
			x(16) = 0;  // qy_dot
			x(17) = 0;  // qz_dot

			// Initialize the filter
			iBody->filter.init(x,P0);

			// Initialize ROS topics for broadcasting data
			std::string SubjectName = std::string(MyClient.GetSubjectName( SubjectNum ).SubjectName)+"a";
			strcpy(iBody->name, SubjectName.c_str());

			std::string posename = "/pose";
			posename = iBody->name + posename;
			std::string velname = "/vel";
			velname = iBody->name + velname;
			std::string accelname = "/accel";
			accelname = iBody->name + accelname;
			
			std::cout << std::endl << std::endl << "Broadcasting data for:" << std::endl;
			std::cout << posename << std::endl;
			std::cout << velname << std::endl;
			std::cout << accelname << std::endl;

			pose_pub[SubjectNum] = broadcast_node.advertise<geometry_msgs::PoseStamped>(posename, 0);
			twist_pub[SubjectNum] = broadcast_node.advertise<geometry_msgs::TwistStamped>(velname, 0);
			accel_pub[SubjectNum] = broadcast_node.advertise<geometry_msgs::Vector3Stamped>(accelname, 0);

			std::string comma = ",";
			viconData.data += iBody->name + comma;
		}
		names_pub  = broadcast_node.advertise<std_msgs::String>("ViconData",0);

		for (unsigned int n = 0; n < nBodies; n++){
			// Initialize flip
			flip.at(n) = 1.0;
		}

		// Observation vector
		Vector z_K(7);  // (x,y,z,qo,qx,qy,qz) in meters and radians

		// Control vector (N/A for this filter, but still needed for the step() function
		Vector u_K(0);

		Vector xest(17);
		Matrix Pest(17,17);
		printf("Finished Kalman setup.\n");

		/****************  END KALMAN FILTER SETUP  *******************/

		// Start Loop here!
        double x_f, y_f, z_f;
        double qo_filt, qx_filt, qy_filt, qz_filt;
        double qo_dfilt, qx_dfilt, qy_dfilt, qz_dfilt;
        double x_dot, y_dot, z_dot, x_ddot, y_ddot, z_ddot;

		while(not ABORT)
		{

			// Get a frame
			//Waiting for a new frame...
			while( MyClient.GetFrame().Result != Result::Success )
			{}
			
			//std::cout << "Frame Number: " << _Output_GetFrameNumber.FrameNumber << std::endl;


			/* // Get the latency
			std::cout << "Latency: " << MyClient.GetLatencyTotal().Total << "s" << std::endl;

			for( unsigned int LatencySampleIndex = 0 ; LatencySampleIndex < MyClient.GetLatencySampleCount().Count ; ++LatencySampleIndex )
			{
			std::string SampleName  = MyClient.GetLatencySampleName( LatencySampleIndex ).Name;
			double      SampleValue = MyClient.GetLatencySampleValue( SampleName ).Value;

			std::cout << "  " << SampleName << " " << SampleValue << "s" << std::endl;
			}
			std::cout << std::endl;*/
			
			// Calcuate wall time and average to make sure we are getting vicon data at the expected data rate
			double time = ros::Time().now().toSec();
			static double time_old = time - DT;
			double measuredDT = time - time_old;
			time_old = time;
			static double measuredDTold = 0;
			// average
			measuredDT = measuredDTold + 0.1*(measuredDT - measuredDTold);
			measuredDTold = measuredDT;
			bool shouldBroadcast = true;
			if (fabs(measuredDT - DT) > 0.002) {
				shouldBroadcast = false;
			}

			//static int cntmark = 0;
			//if (cntmark == 100) {
			//	cntmark = 0;
			//	std::cout << elapsedTime << std::endl;
			//}
			//cntmark++;

			// Count the number of subjects, make sure it matches
			// If it doesnt, reset everything
			if(MyClient.GetSubjectCount().SubjectCount != nBodies)
				break;

			int body_number		= 9;
			int SubjectIndex	= -1;
			for(iBody = Bodies.begin(); iBody != Bodies.end(); iBody++) {
				body_number++;
				SubjectIndex++;
				
				//Get the name
				std::string SubjectName = MyClient.GetSubjectName( SubjectIndex ).SubjectName;
				strcpy(iBody->name, SubjectName.c_str());

				//Need these as a reference
				// We only care about segment #0
				unsigned int SegmentIndex = 0;
				std::string SegmentName = MyClient.GetSegmentName( SubjectName, SegmentIndex ).SegmentName;

				//However we're only going to take the first segment of the body
				Output_GetSegmentGlobalTranslation _Output_GetSegmentGlobalTranslation = 
					MyClient.GetSegmentGlobalTranslation( SubjectName, SegmentName );
				iBody->x  = _Output_GetSegmentGlobalTranslation.Translation[ 0 ]/1000.0;
				iBody->y  = _Output_GetSegmentGlobalTranslation.Translation[ 1 ]/1000.0;
				iBody->z  = _Output_GetSegmentGlobalTranslation.Translation[ 2 ]/1000.0;
				bool occluded = _Output_GetSegmentGlobalTranslation.Occluded;

				Output_GetSegmentGlobalRotationQuaternion _Output_GetSegmentGlobalRotationQuaternion = 
					MyClient.GetSegmentGlobalRotationQuaternion( SubjectName, SegmentName );
				iBody->qx =  _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 0 ];
				iBody->qy =  _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 1 ];
				iBody->qz =  _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 2 ];
				iBody->q0 =  _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 3 ];

				//CHECK TO MAKE SURE OBJECT STILL IN VIEW
				if(occluded) {
					//Object no longer tracking, send safe data
					if(iBody->x_prev != iBody->x_prev || 
						iBody->y_prev!= iBody->y_prev  || 
						iBody->z_prev!= iBody->z_prev )
					{
						iBody->x_prev=0.0;
						iBody->y_prev=0.0;
						iBody->z_prev=0.0;

					}
					if(iBody->x_prev>100||iBody->y_prev>100||iBody->z_prev>100)
					{
						iBody->x_prev=0.0;
						iBody->y_prev=0.0;
						iBody->z_prev=0.0;
					}


					iBody->x = iBody->x_prev;
					iBody->y = iBody->y_prev;
					if(iBody->z_prev > 0 && iBody->z_prev<6)
						iBody->z_prev += 0.004;
					iBody->z = iBody->z_prev;  //High, will make quad "fly" to ground
    
					iBody->q0 = iBody->q0_prev;
					iBody->qx = iBody->qx_prev;
					iBody->qy = iBody->qy_prev;
					iBody->qz = iBody->qz_prev;
					
				}

				
				// This check makes the quaternions continuous so the derivatives are smooth
				//	Since the 2-norm of the angle axis vector is always unity a jump between
				//    steps will indicate appx. -1.
				double dotprod = iBody->q0*iBody->q0_prev + iBody->qx*iBody->qx_prev + iBody->qy*iBody->qy_prev + iBody->qz*iBody->qz_prev;
				if (dotprod < 0.0) {
					flip.at(SubjectIndex) = -flip.at(SubjectIndex);
				}

				
				double qo_cont, qx_cont, qy_cont, qz_cont;

				// Switch from ENU to NED
				//qo_cont	=  flip.at(SubjectIndex)*iBody->q0;
				//qx_cont	=  flip.at(SubjectIndex)*iBody->qy;	//ay switches with ax
				//qy_cont	=  flip.at(SubjectIndex)*iBody->qx;
				//qz_cont	= -flip.at(SubjectIndex)*iBody->qz;	//az is opposite

				qo_cont	=  flip.at(SubjectIndex)*iBody->q0;
				qx_cont	= flip.at(SubjectIndex)*iBody->qx;
				qy_cont	= flip.at(SubjectIndex)*iBody->qy;
				qz_cont	= flip.at(SubjectIndex)*iBody->qz;

				
				/******************  KALMAN FILTERING  *********************/
				// added by sahrens 10/3/2007
				z_K(1) = iBody->x;
				z_K(2) = iBody->y;
				z_K(3) = iBody->z;
				z_K(4) = qo_cont;
				z_K(5) = qx_cont;
				z_K(6) = qy_cont;
				z_K(7) = qz_cont;
				
				// Run the filter!
				iBody->filter.step(u_K,z_K);
				
				// Display stuff
				xest	= iBody->filter.getX();
				Pest	= iBody->filter.calculateP();

				// Extract states
				x_f       = xest(1);   // position
				y_f       = xest(2);
				z_f       = xest(3);
				x_dot     = xest(4);   // velocity
				y_dot     = xest(5);
				z_dot     = xest(6);
				x_ddot    = xest(7);   // acceleration
				y_ddot    = xest(8);
				z_ddot    = xest(9);
				qo_filt   = xest(10);  // quaternion
				qx_filt	  = xest(11);
				qy_filt	  = xest(12);
				qz_filt	  = xest(13);
				qo_dfilt  = xest(14);  // quaternion rate
				qx_dfilt  = xest(15);
				qy_dfilt  = xest(16);
				qz_dfilt  = xest(17);

				// Renormalize - this should not be an issue but just in case the process noise covariance is not large enough
				double norm_q = sqrt(qo_filt*qo_filt + qx_filt*qx_filt + qy_filt*qy_filt + qz_filt*qz_filt);
				qo_filt	= qo_filt/norm_q;
				qx_filt	= qx_filt/norm_q;
				qy_filt	= qy_filt/norm_q;
				qz_filt	= qz_filt/norm_q;

				// Switch from ENU to NED
				//qo_cont	=  flip.at(SubjectIndex)*iBody->q0;
				//qx_cont	=  flip.at(SubjectIndex)*iBody->qy;	//ay switches with ax
				//qy_cont	=  flip.at(SubjectIndex)*iBody->qx;
				//qz_cont	= -flip.at(SubjectIndex)*iBody->qz;	//az is opposite
				

				/****************  END KALMAN FILTERING  *******************/

				//Transformations are adopted from:
				//Review of Attitude Representations Used for Aircraft Kinematics
				//Journal of Aircraft Vol. 38, No.4, July-August 2001
				// unless otherwise noted
				/*phi_f		= atan2(2*(qo_filt*qx_filt + qy_filt*qz_filt),(qo_filt*qo_filt + qz_filt*qz_filt - qx_filt*qx_filt - qy_filt*qy_filt));
				theta_f		= asin (2*(qo_filt*qy_filt - qx_filt*qz_filt));
				psi_f		= atan2(2*(qo_filt*qz_filt + qx_filt*qy_filt),(qo_filt*qo_filt + qx_filt*qx_filt - qy_filt*qy_filt - qz_filt*qz_filt));

				double phi   = atan2(2*(qo_cont*qx_cont + qy_cont*qz_cont),(qo_cont*qo_cont+ qz_cont*qz_cont- qx_cont*qx_cont- qy_cont*qy_cont));
				double theta = asin (2*(qo_cont*qy_cont - qx_cont*qz_cont));
				double psi   = atan2(2*(qo_cont*qz_cont + qx_cont*qy_cont),(qo_cont*qo_cont+ qx_cont*qx_cont- qy_cont*qy_cont- qz_cont*qz_cont));*/

				// convert quaternion/quaternion rates to body attitude rates
				// Aircraft Control and Simulation - Stevens and Lewis
				double p = 2*( qo_filt*qx_dfilt + qz_filt*qy_dfilt - qy_filt*qz_dfilt - qx_filt*qo_dfilt);
				double q = 2*(-qz_filt*qx_dfilt + qo_filt*qy_dfilt + qx_filt*qz_dfilt - qy_filt*qo_dfilt);
				double r = 2*( qy_filt*qx_dfilt - qx_filt*qy_dfilt + qo_filt*qz_dfilt - qz_filt*qo_dfilt);
				
				// Populate ROS messages
				geometry_msgs::PoseStamped POSE;
				geometry_msgs::TwistStamped TWIST;
				geometry_msgs::Vector3Stamped ACCEL;

				ros::Time t = ros::Time().now();
				static double t_start = t.toSec();

				POSE.header.stamp = t;
				POSE.header.frame_id = iBody->name;
				POSE.pose.position.x = y_f;			// ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
				POSE.pose.position.y = -x_f;
				POSE.pose.position.z = z_f;
				POSE.pose.orientation.w = qo_filt;
				POSE.pose.orientation.x = qy_filt;	// ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
				POSE.pose.orientation.y = -qx_filt;
				POSE.pose.orientation.z = qz_filt;

				TWIST.header.stamp = t;
				TWIST.header.frame_id = iBody->name;
				TWIST.twist.linear.x = y_dot;
				TWIST.twist.linear.y = -x_dot;
				TWIST.twist.linear.z = z_dot;
				TWIST.twist.angular.x = q;
				TWIST.twist.angular.y = -p;
				TWIST.twist.angular.z = r;

				ACCEL.header.stamp = t;
				ACCEL.header.frame_id = iBody->name;
				ACCEL.vector.x = y_ddot;   // ROS uses a north/west/up convention -- not particularly "aero-savvy", but is consistent with the robotics community
				ACCEL.vector.y = -x_ddot;
				ACCEL.vector.z = z_ddot;

				// log data DEBUG!!!
				if (std::strcmp(iBody->name,"RC01")==0){
					logFile << t.toSec() - t_start << ","
							<< _Output_GetSegmentGlobalTranslation.Translation[ 0 ]/1000.0 << ","
							<< _Output_GetSegmentGlobalTranslation.Translation[ 1 ]/1000.0 << ","
							<< _Output_GetSegmentGlobalTranslation.Translation[ 2 ]/1000.0 << ","
							<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 3 ] << ","
							<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 0 ] << ","
							<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 1 ] << ","
							<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[ 2 ] << ","
							<< x_f << "," << y_f << "," << z_f << ","
							<< qo_filt << "," << qx_filt << "," << qy_filt << "," << qz_filt << ","
							<< x_dot << "," << y_dot << "," << z_dot << ","
							<< p << "," << q << "," << r << std::endl;
				}

				// Broadcast ROS data
				if (shouldBroadcast) {
					pose_pub[SubjectIndex].publish(POSE);
					twist_pub[SubjectIndex].publish(TWIST);
					accel_pub[SubjectIndex].publish(ACCEL);
				}

				// update previous values
				iBody->q0_prev = iBody->q0;
				iBody->qx_prev = iBody->qx;
				iBody->qy_prev = iBody->qy;
				iBody->qz_prev = iBody->qz;
				iBody->x_prev  = x_f;
				iBody->y_prev  = y_f;
				iBody->z_prev  = z_f;
			}


			// Broadcast string of data with all the vehicle names in it
			if (shouldBroadcast)
				names_pub.publish(viconData);

			static int errorCnt = 0;
			errorCnt++;
			if (errorCnt > 100 && !shouldBroadcast) {
				errorCnt = 0;
				std::cout << "Vicon data rate: " << measuredDT << 
				" does not match expected data rate: " << DT << std::endl;
			}
		}
	}

	// Disconnect and dispose
	MyClient.Disconnect();
	logFile.close();

}

//## Custom Control-C handler
void controlC(int sig)
{
  ABORT = true;	// ros::shutdown called in Quadrotor::sendCmd() function
}
