#ifndef TCPPORT_HPP_
#define TCPPORT_HPP_

#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
//#include <termios.h>
#include <fcntl.h>
//#include <winsock2.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class TCPPort
{

        int    port;
        int    sock;
        struct sockaddr_in address;

public:

//		int   initWinsock();
        int   tcpInit(std::string addr, int portnum);
        int   tcpReceiveI(char* str, int sz);
        int   tcpReceiveI(char* str);
        bool  tcpReceive(char *pBuffer, int BufferSize);
        bool  tcpReceive(int &Val);
        bool  tcpReceive(unsigned int & Val);
        bool  tcpReceive(double &Val);

        int   tcpSend(char* pkt);
        int   tcpSend(char* pkt, int sz);
        void  tcpClose();

};

#endif /*TCPPORT_HPP_*/
