#ifndef VICONCLASSES_HPP_
#define VICONCLASSES_HPP_

#include <assert.h>
//#include <endian.h>
//#include <errno.h>
//#include <fcntl.h>
#include <math.h>
//#include <netdb.h>
//#include <pthread.h>
//#include <sched.h>
//#include <semaphore.h>
//#include <signal.h>
//#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <time.h>
//#include <termios.h>
//#include <unistd.h>

//#include <arpa/inet.h>
//#include <netinet/in.h>

//#include <sys/ipc.h>
//#include <sys/poll.h>
//#include <sys/shm.h>
//#include <sys/sem.h>
//#include <sys/socket.h>
//#include <sys/stat.h>
//#include <sys/time.h>
//#include <sys/types.h>
//#include <sys/wait.h>

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <limits>

#pragma once

#include "trackerFilter.h"

class ClientCodes
{
public:
        enum EType
        {
                ERequest,
                EReply
        };

        enum EPacket
        {
                EClose,
                EInfo,
                EData,
                EStreamOn,
                EStreamOff
        };

        static const std::vector< std::string > MarkerTokens;
        static const std::vector< std::string > BodyTokens;

        static std::vector< std::string > MakeMarkerTokens()
        {
                std::vector< std::string > v;
                v.push_back("<P-X>");
                v.push_back("<P-Y>");
                v.push_back("<P-Z>");
                v.push_back("<P-O>");
                return v;
        }

        static std::vector< std::string > MakeBodyTokens()
        {
                std::vector< std::string > v;
                v.push_back("<A-X>");
                v.push_back("<A-Y>");
                v.push_back("<A-Z>");
                v.push_back("<T-X>");
                v.push_back("<T-Y>");
                v.push_back("<T-Z>");
                return v;
        }

struct CompareNames : std::binary_function<std::string, std::string, bool>
        {
                bool operator()(const std::string & a_S1, const std::string & a_S2) const
                {
                        std::string::const_iterator iS1 = a_S1.begin();
                        std::string::const_iterator iS2 = a_S2.begin();

                        while(iS1 != a_S1.end() && iS2 != a_S2.end())
                                if(toupper(*(iS1++)) != toupper(*(iS2++)))
                                        return false;

                        return a_S1.size() == a_S2.size();
                }
        };



};


// *******************************
class MarkerChannel
{
public:
        std::string Name;

        int X;
        int Y;
        int Z;
        int O;

        MarkerChannel(std::string & a_rName) : X(-1), Y(-1), Z(-1), O(-1), Name(a_rName)
        {}

        int & operator[](int i)
        {
                switch(i) {
                case 0:
                        return X;
                case 1:
                        return Y;
                case 2:
                        return Z;
                case 3:
                        return O;
                default:
                        assert(false);
                        return O;
                }
        }

        int operator[](int i) const
        {
                switch(i) {
                case 0:
                        return X;
                case 1:
                        return Y;
                case 2:
                        return Z;
                case 3:
                        return O;
                default:
                        assert(false);
                        return -1;
                }
        }


        bool operator==(const std::string & a_rName)
        {
                ClientCodes::CompareNames comparitor;
                return comparitor(Name, a_rName);
        }

};


// *******************************
class MarkerData
{
public:
        double	X;
        double	Y;
        double	Z;
        bool	Visible;
};


// *******************************
class BodyChannel
{
public:
        std::string Name;

        int TX;
        int TY;
        int TZ;
        int RX;
        int RY;
        int RZ;

        BodyChannel(std::string & a_rName) : RX(-1), RY(-1), RZ(-1), TX(-1), TY(-1), TZ(-1), Name(a_rName)
        {}

        int & operator[](int i)
        {
                switch(i) {
                case 0:
                        return RX;
                case 1:
                        return RY;
                case 2:
                        return RZ;
                case 3:
                        return TX;
                case 4:
                        return TY;
                case 5:
                        return TZ;
                default:
                        assert(false);
                        return TZ;
                }
        }

        int operator[](int i) const
        {
                switch(i) {
                case 0:
                        return RX;
                case 1:
                        return RY;
                case 2:
                        return RZ;
                case 3:
                        return TX;
                case 4:
                        return TY;
                case 5:
                        return TZ;
                default:
                        assert(false);
                        return -1;
                }
        }

        bool operator==(const std::string & a_rName)
        {
                ClientCodes::CompareNames comparitor;
                return comparitor(Name, a_rName);
        }
};


// *******************************
class BodyData
{
public:
        // Representation of body translation
        double TX;
        double TY;
        double TZ;

        CTrackerFilter filter;

        double psi_offset, psi_prev;
        int    veh_id;

        // Representation of body rotation in Quaternions
        double QX;
        double QY;
        double QZ;
        double QW;
};




// *******************************
struct Marker
{

        double x;
        double y;
        double z;

};



// *******************************
struct Body
{
        int timestamp;
        char   name[128];//std::string name;
        int    ID;
        double x;        // x position (m)
        double y;
        double z;
        double roll;     // roll angle (rad)
        double pitch;
        double yaw;
        double dx;       // x velocity (m/s)
        double dy;
        double dz;
        double p;        // roll angle rate (rad/s)
        double q;
        double r;
        double qx;       // quaternion attitude
        double qy;
        double qz;
        double qw;

        Body()
        {
                timestamp = 0;
                strcpy(name,"n/a");
                ID        = 0;
                x         = 0.0;
                y         = 0.0;
                z         = 0.0;
                roll      = 0.0;
                pitch     = 0.0;
                yaw       = 0.0;
                dx        = 0.0;
                dy        = 0.0;
                dz        = 0.0;
                p         = 0.0;
                q         = 0.0;
                r         = 0.0;
                qx        = 0.0;
                qy        = 0.0;
                qz        = 0.0;
                qw        = 0.0;
        }
};





#endif /*VICONCLASSES_HPP_*/
