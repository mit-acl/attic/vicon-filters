/*!
 * \file vicon.h
 *
 * Pulls data from vicon, filters it, and broadcasts it on the network
 *
 * Created on: Oct 16, 2013
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#ifndef VICON_H_
#define VICON_H_

#include <Client.h>
#include <vector>
#include <map>
#include <string>

#include "body.h"
#include "defines.h"

// ROS includes
#include <ros/ros.h> // standard ros header
#include <geometry_msgs/PoseStamped.h>  // for the position and orientation
#include <geometry_msgs/TwistStamped.h> // for the velocity and angular velocities
#include <geometry_msgs/Vector3Stamped.h> // for the acceleration

/**
 *  Pulls data from vicon, filters it, and broadcasts it on the network
 */
class Vicon
{
public:
	Vicon();
	~Vicon();

	void updateBodyMap();
	bool runFilter();
	void screenPrint();
	
private:
	ViconDataStreamSDK::CPP::Client MyClient;
	std::vector<Body> Bodies;
	std::map<std::string,Body> BodyMap;

	unsigned int numCurrentBodies;

	double t_prev;
	double dt_filt;
	// bool shouldBroadcast;

	double calcDT();
	void initClient();
	geometry_msgs::Pose getViconData(std::string name, bool &occluded, unsigned int &marker_count, unsigned int &num_of_marker);
	geometry_msgs::Pose getViconData(std::string name, bool &occluded);
	void printLatency();

};

#endif /* VICON_H_ */
