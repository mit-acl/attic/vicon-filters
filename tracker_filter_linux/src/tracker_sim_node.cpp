#include <ros/ros.h> // standard ros header
#include <signal.h>

#include "body.h"

// Msgs
#include <tracker_filter_linux/ViconRaw.h>
#include <geometry_msgs/Pose.h>
#include <std_msgs/Bool.h>

// #include <geometry_msgs/PoseStamped.h>  // for the position and orientation

class TrackerSim{
public:
  ros::NodeHandle nh;
  ros::Subscriber sub;
  std::string veh_name;
  double dt;
  Body body;

  TrackerSim(int argc, char** argv)
  {
    // Initialize the node
    nh = ros::NodeHandle("~");

    // Get parameters
    // nh.getParam("veh_name",veh_name);
    nh.getParam("dt",dt);
    
    // Subscribe
    // std::string topic_name = "/" + veh_name + "/viconraw";
    sub = nh.subscribe("input", 1, &TrackerSim::callback, this);

    // Initialzie the filter body
    body.resetStates();
    body.setName(ros::this_node::getName());
    body.initPublishers();
  }

  void callback(const tracker_filter_linux::ViconRaw& viconraw)
  {
    body.update(dt, viconraw.measurement, viconraw.occluded.data,
      viconraw.marker_count.data, viconraw.num_of_markers.data);
    body.broadcastData();
  }
};

int main (int argc, char** argv)
{
  ros::init (argc, argv, "tracker_sim");
  TrackerSim tracker_sim(argc, argv);
  ros::spin();
  return 0;
}
