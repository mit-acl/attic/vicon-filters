#include "viconClasses.hpp"

const std::vector< std::string > ClientCodes::MarkerTokens = MakeMarkerTokens();
const std::vector< std::string > ClientCodes::BodyTokens = MakeBodyTokens();
