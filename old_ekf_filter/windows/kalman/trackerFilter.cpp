// This file defines a 17-state (pos, vel, acc, quat, quat_dot) Kalman filter for tracking an object
// with 6-DOF

#include "trackerFilter.h"
double CTrackerFilter::deltaT=DT;
CTrackerFilter::CTrackerFilter()
{

        // The function setDim() sets the number of states, the number of inputs,
        // the number of process noise random variables, the number of measures and
        //  the number of measurement noise random variables.
        //setDim(15, 0, 6, 6, 6);  //way bigger
        setDim(17, 0, 7, 7, 7); //changed by: fsobolic
}


// Construct the state transition matrix
// x[k+1] = A x[k] + W w[k]
// 17x17
void CTrackerFilter::makeBaseA()
{
        //note: pos[k+1] = pos[k] + dt*vel[k] != pos[k] + dt*vel[k] + dt^2*acc[k]
        // this might have time delay, but otherwise we could end up adding 2*dt^2*acc
        for(int i = 1; i <= 17; i++) {
                for(int j = 1; j <= 17; j++) {
                        if(i == j) {
                                A(i,j) = 1.0;
                        } else {
                                switch(j) {
                                case 1:
                                case 2:
                                case 3:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                        A(i,j) = 0.0;  //measured states
                                        break;
                                default:
                                        if(j == i + 3) {
                                                A(i,j) = deltaT;   //integrator spots
                                        } else {
                                                A(i,j) = 0.0;  //empty slots
                                        }
                                        break;
                                }
                        }
                }
        }
        A(13,17) = deltaT;
        A(12,16) = deltaT;
        A(11,15) = deltaT;
        A(10,14) = deltaT;
}


// Construct the observation matrix
// z[k] = H x[k] + V v[k]
// 7x17
void CTrackerFilter::makeBaseH()
{
        double h = 0.0;
        for(int j = 1; j <= 17; j++) {
                for(int i = 1; i <= 7; i++) {
                        if(i%6 == j%6) {  //diagonals
                                switch(j) {
                                case 4:
                                case 13:
                                case 20:
                                        h = 0.0;  //computed states
                                        break;
                                case 10:
                                        h = 0.0;  //sensed states
                                        break;
                                case 16:
                                        h = 0.0;
                                        break;
                                }
                                H(i,j) = h;
                        } else {
                                H(i,j) = 0.0;      //empty
                        }
                }
        }
        H(1,1)	= 1;
        H(2,2)	= 1;
        H(3,3)	= 1;
        H(4,10)	= 1;
        H(5,11)	= 1;
        H(6,12)	= 1;
        H(7,13)	= 1;

}

// Measurement noise matrix
// z[k] = H x[k] + V v[k]
// 7x7
void CTrackerFilter::makeBaseV()
{
        for(int i = 1; i <= 7; i++) {
                for(int j = 1; j <= 7; j++) {
                        if(i == j) {
                                V(i,j) = 1.0;
                        } else {
                                V(i,j) = 0.0;
                        }
                }
        }
}

// Measurement noise (v) covariance
// 7x7
void CTrackerFilter::makeBaseR()
{

        // covariance of each {x,y,z} measurement, in m^2
        const double d = POS_MEASUREMENT_COV; //4.0043e-8;  // from matlab d = cov(vicon.pos)
        // covariance of each angular measurement, in rad^2
        const double a = ATT_MEASUREMENT_COV; //6.315e-7;  // from matlab a = cov(vicon.att)

        double r = d;

        for(int i = 1; i <= 7; i++) {
                for(int j = 1; j <= 7; j++) {
                        if(i == j) {
                                if(i == 4) {  // switch to attitude
                                        r = a;
                                }
                                R(i,j) = r;
                        } else {
                                R(i,j) = 0.0;
                        }
                }
        }
}


// Process noise input matrix
// x[k+1] = A x[k] + W w[k]
// 17x7
void CTrackerFilter::makeBaseW()
{ // recheck your work here
        double w = pow(deltaT,3.0)/6.0;
        // position input
        for(int i = 1; i <= 17; i++) {
                for(int j = 1; j <= 3; j++) {
                        if(i%3 == j%3) { //diagonals
                                switch(i) {
                                case 4:
                                        w = deltaT*deltaT/2;
                                        break;
                                case 7:
                                        w = deltaT;
                                        break;
                                case 10:
                                        w = 0;
                                        break;
                                }
                                W(i,j) = w;
                        } else {
                                W(i,j) = 0;
                        }
                }
        }

        // quaternion input
        for(int i = 1; i <= 17; i++) {
                for(int j = 4; j <= 7; j++) {
                        W(i,j) = 0;
                }
        }
        w			= deltaT*deltaT/2;
        W(10,4)	= w;
        W(11,5)	= w;
        W(12,6)	= w;
        W(13,7)	= w;

        w			= deltaT;
        W(14,4)	= w;
        W(15,5)	= w;
        W(16,6)	= w;
        W(17,7)   = w;
}

// Process noise (w) covariance
// 7x7
void CTrackerFilter::makeBaseQ()
{

        // covariance of each {x,y,z} jerk disturbance, in (m/s^3)^2
        const double d = POS_PROCESS_COV*POS_PROCESS_COV;//(10.0)*(10.0);  // 10.0m/s^3?  50?
        // covariance of each angular acceleration disturbance, in (rad/s^2)^2
        const double a = ATT_PROCESS_COV*ATT_PROCESS_COV;//(1000.0*M_PI/180.0)*(1000.0*M_PI/180.0);  //1000 degrees/s^2?  1500?

        double q = d;

        for(int i = 1; i <= 7; i++) {
                for(int j = 1; j <= 7; j++) {
                        if(i == j) {
                                if(i == 4) {  // switch to attitude
                                        q = a;
                                }
                                Q(i,j) = q;
                        } else {
                                Q(i,j) = 0.0;
                        }
                }
        }
}


// Explicit representation of the process x[k+1] = A x[k]
void CTrackerFilter::makeProcess()
{

        Vector x_(x.size());

        x_(1) = x(1) + deltaT*x(4);     // position = pos[old] + dt*vel
        x_(2) = x(2) + deltaT*x(5);
        x_(3) = x(3) + deltaT*x(6);
        x_(4) = x(4) + deltaT*x(7);    // velocity = vel[old] + dt*acc
        x_(5) = x(5) + deltaT*x(8);
        x_(6) = x(6) + deltaT*x(9);
        x_(7) = x(7);				     //acceleration
        x_(8) = x(8);
        x_(9) = x(9);
        x_(10) = x(10) + deltaT*x(14); // quat = quat[old] + dt*dquat
        x_(11) = x(11) + deltaT*x(15);
        x_(12) = x(12) + deltaT*x(16);
        x_(13) = x(13) + deltaT*x(17);
        x_(14) = x(14);                // quaternion rate
        x_(15) = x(15);
        x_(16) = x(16);
        x_(17) = x(17);

        x.swap(x_);
}

// Explicit representation of the measurement z[k+1] = H x[k]
void CTrackerFilter::makeMeasure()
{

        z(1) = x(1);  //position
        z(2) = x(2);
        z(3) = x(3);
        z(4) = x(10); //quaternion
        z(5) = x(11);
        z(6) = x(12);
        z(7) = x(13);

}
