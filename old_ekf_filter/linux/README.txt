Notes:

Using ViconSDK version 1.3, Linux64 for boost 1.46.1

After unpacking the ViconSDK .tar file navigate to the version for Linux64 using boost 1.46.1:  

cd Vicon_SDK_1.3_Linux/Linux64-boost-1.46.1

There should be several folders named with a release date and release number (e.g. 20130116_57137h). cd into the latest release folder:   

cd 20130116_57137h/Release

From that folder, copy all the *.so* files into the /usr/local/lib folder and the .h file
into the /usr/local/include folder

sudo cp *.so* /usr/local/lib
sudo cp *.h /usr/local/include

sudo ldconfig
